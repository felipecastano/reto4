from __future__ import unicode_literals

from django.apps import AppConfig


class Reto4Config(AppConfig):
    name = 'reto4'
