from django import template
register = template.Library()

from reto4.models import *

@register.simple_tag
def section(seccion):
    return Customs.objects.filter(title=seccion).first()

@register.simple_tag
def banners(value):
    banners = Banner.objects.filter(relacion__categoria=value)
    return banners
