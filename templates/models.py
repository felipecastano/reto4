from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Categorias(models.Model):
    categoria = models.CharField(max_length=30)

class Banner(models.Model):
    relacion = models.ForeignKey(Categorias, on_delete=models.CASCADE, verbose_name='categoria id')
    titulo = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=1000)
    imagen = models.FileField(upload_to='uploads', null=True)

class Customs(models.Model):
    title = models.CharField(max_length=30, null=True)
    description = models.TextField()
