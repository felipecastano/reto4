reto4-umaster

### Installing
* Crear ambiente virtual ($virtualenv ambiente)
* Ingresar al ambiente virtual: source ambiente/bin/activate
* clonar proyecto: git clone git@gitlab.com:yullyguzman/reto4Django.git
* cd reto4Django/
* Instalar requerimientos:  **pip install -r requirements.txt**
* python manage.py runserver
